
# Master Token

  

This is an **ERC-20** smart contract, with 5 main features.

  
  

# Prequisites

  

Before continuing to write some code, it is essential to install these first:
  

## NodeJS >= 5.0 and npm

**NodeJS** is a run-time environment which includes everything you need to execute a program written in JavaScript. It’s used for running scripts on the server to render content before it is delivered to a web browser.
### Install
- **MacOS**: https://nodejs.org/dist/v16.0.0/node-v16.0.0.pkg
- **Windows**: https://nodejs.org/dist/v16.0.0/node-v16.0.0-x86.msi
- **Linux**: Follow instructions on [GitHub](https://github.com/nodesource/distributions/blob/master/README.md). Please do not install via any package managers!

More information, please visit NodeJS [homepage](https://nodejs.org/en/). **npm** is automatically installed with NodeJS.

Check NodeJS and npm version:
>node --version 
>npm --version 

## Truffle

**Truffle** is the most popular development framework for Ethereum with a mission to make your life a whole lot easier.

### Install
>npm install -g truffle

### Check truffle version
>truffle --version

  

## Metamask

**MetaMask** is a software cryptocurrency wallet used to interact with the Ethereum blockchain.
### Install
Metamask is only available on Android, iOS and Chrome. Chrome is required for [Metamask](https://metamask.io/download.html) if you want to install it on PC.


  

## Ganache
**Ganache** is a personal blockchain for rapid Ethereum and Corda distributed application development. You can use Ganache across the entire development cycle; enabling you to develop, deploy, and test your dApps in a safe and deterministic environment.

Ganache homepage: https://www.trufflesuite.com/ganache.
## Text Editor

You can use **Visual Studio Code** or **Sublime Text**. If you are using Sublime Text, install **Ethereum Package** via **Package Control**.
