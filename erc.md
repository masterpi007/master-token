# ETHEREUM 

Ethereum is a distributed state machine(an abstract machine that can be in exactly one of a finite number of state at any given time) . Everyone who participates in the Ethereum network (every Ethereum node) keeps a copy of the state of this computer, any participant can broadcast a request for this computer to perform arbitrary computation (transaction request). Through proof of work, a new valid block ( contains transactions and state) is added to the blockchain. This causes a state change in the EVM, which is committed and propagated throughout the entire network. 

Requests for computation are called transaction requests; the record of all transactions as well as the EVM’s present state is stored in the blockchain, which in turn is stored and agreed upon by all nodes.

Ethereum's state is a large data structure which holds not only all accounts and balances, but a _machine state_, which can change from block to block according to a pre-defined set of rules executed via arbitrary machine code

![A diagram showing the make up of the EVM](https://d33wubrfki0l68.cloudfront.net/1e28992f5356f5cbe6a6cd4ae55afb567603ef5d/b91ce/static/e8aca8381c7b3b40c44bf8882d4ab930/302a4/evm.png)

 ## THE ETHEREUM STATE TRANSITION FUNCTION
The EVM behaves as a mathematical function would: Given an input, it produces a deterministic output 
EVM's function : Y(S, T)= S'
old valid state `S`
new set of valid transactions `T`
new valid output state `S'`

the state is an enormous data structure called a  modified Merkle Patricia Trie, which keeps all accounts (an entity with an ether (ETH) balance that can send transactions on Ethereum, including Externally-owned and Contract) linked by hashes and reducible to a single root hash stored on the blockchain.

## Transactions
-Transactions are cryptographically signed instructions from accounts
-2 type :  

1. contract creation : creates new contract account containing compiled [smart contract](https://ethereum.org/en/developers/docs/smart-contracts/anatomy/) bytecode
2. message calls : makes a message call to that contract, executes its bytecode

## WHAT IS GAS?
Gas refers to the unit that measures the amount of computational effort required to execute specific operations on the Ethereum network.

gas fees are paid in Ethereum's native currency, ether (ETH)

each transaction is required to set a limit to how many computational steps of code execution it can use

any gas not used in a transaction is returned to the user

## WHAT IS A SMART CONTRACT?
A "smart contract" is simply a program that runs on the Ethereum blockchain. It's a collection of code (its functions) and data that resides at a specific address on the Ethereum blockchain

Smart contracts are a type of Ethereum account. This means they have a balance and they can send transactions over the network. However they're not controlled by a user, instead they are deployed to the network and run as programmed. User accounts can then interact with a smart contract by submitting transactions that execute a function defined on the smart contract ( read the passage below to get it) . Smart contracts can define rules, like a regular contract, and automatically enforce them via the code

#### A quick recap on Ethereum tokens
Unlike ETH (Ethereum’s native cryptocurrency), ERC-20 tokens aren’t held by accounts. The tokens only exist inside a contract, which is like a self-contained database. It specifies the rules for the tokens (i.e., name, symbol, divisibility) and keeps a list that maps users’ balances to their Ethereum addresses.

To move tokens, users must send a transaction to the contract asking it to allocate some of their balance elsewhere. For example, if Alice wants to send 5,000 BinanceAcademyTokens to Bob, she calls a function inside the BinanceAcademyToken smart contract asking it to do so.

[Here's a real-world example](https://etherscan.io/tx/0x4cd231b1992498e238b8f5729fde9d1f622f92a47689619cb48bd6790184f8af) of the above on Etherscan: someone is making a call to the BUSD contract. You can see tokens were transferred, and a fee has been paid, even though the Value field shows that 0 ETH has been sent.




Basically, Ethereum smart contracts are made of a contract code and two public keys. The first public key is the one provided by the creator of the contract. The other key represents the contract itself, acting as a digital identifier that is unique to each smart contract.

## FUNCTIONS

In the most simplistic terms, functions can get information or set information in response to incoming transactions.

There are two types of function calls:

-   `internal`  – these don't create an EVM call
    -   Internal functions and state variables can only be accessed internally (i.e. from within the current contract or contracts deriving from it)
-   `external`  – these do create an EVM call
    -   External functions are part of the contract interface, which means they can be called from other contracts and via transactions. An external function  `f`  cannot be called internally (i.e.  `f()`  does not work, but  `this.f()`  works).

`constructor` functions are only executed once when the contract is first deployed. Like `constructor` in many class-based programming languages, these functions often initialize state variables to their specified values.

## HOW TO DEPLOY A SMART CONTRACT
To deploy a smart contract, you merely send an Ethereum transaction containing the code of the compiled smart contract without specifying any recipients.
This means you'll need to pay a transaction fee so make sure you have some ETH.
