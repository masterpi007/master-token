var masterToken = artifacts.require("./masterToken.sol");

contract("masterToken",function(accounts) {
    it("SeT tHE tOTaL SuPpLy upOn DEploYmEnt",function() {
        return masterToken.deployed().then(function(instance) {
            tokenInstance = instance;
            return tokenInstance.totalSupply();
        }).then (function(totalSupply) {
            assert.equal(totalSupply.toNumber(),1000000,"Set the total supply to 1000000");
        });
    });
})